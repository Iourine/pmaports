# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Alexander Martinz <amartinz@shiftphones.com>

pkgname=device-shift-axolotl
pkgdesc="SHIFT6mq"
pkgver=1
pkgrel=1
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	linux-postmarketos-qcom-sdm845
	mkbootimg
	postmarketos-base
	postmarketos-update-kernel
	soc-qcom-sdm845
	soc-qcom-sdm845-ucm
"
makedepends="devicepkg-dev"
source="
	deviceinfo
	rootston.ini
"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-phosh
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

nonfree_firmware() {
	pkgdesc="Modem, GPU and WiFi Firmware, also needed for osk-sdl"
	depends="firmware-shift-sdm845 firmware-shift-sdm845-initramfs"
	mkdir "$subpkgdir"
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

sha512sums="
bd30bc057e3145118203460914463794b73c1eaf73a8537a87036d316cf42035861788e7047193c1f26e9d7c69686b8c4ffa2b87882eec9ab7dfcc8edbf12394  deviceinfo
9fef488a655fcbad4fb28c11d7d6cbe385096e766cd99ca59802f1dbc4e3c99dac0ff682549e02fac0b73f7e95db953f3a87c453d1b19b229785e4ffeec515ed  rootston.ini
"
